<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CarsController@index')->middleware('auth');

Route::prefix('cars')->name('cars.')->middleware('auth')->group(function () {
    Route::get('/', 'CarsController@index')->name('index');
    Route::get('/create', 'CarsController@create')->name('create');
    Route::get('{id}', 'CarsController@show')->name('show');
    Route::get('{id}/edit', 'CarsController@edit')->name('edit');
    Route::put('{id}', 'CarsController@update')->name('update');
    Route::post('/', 'CarsController@store')->name('store');
    Route::delete('{id}', 'CarsController@destroy')->name('destroy');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/auth/github/login', 'Auth\\LoginController@redirectToGithub');
Route::any('/auth/github/callback', 'Auth\\LoginController@handleGithubProviderCallback');
