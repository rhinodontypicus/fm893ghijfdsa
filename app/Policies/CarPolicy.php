<?php

namespace App\Policies;

use App\Entity\Car;
use App\Entity\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CarPolicy
{
    use HandlesAuthorization;

    public function create(User $user)
    {
        return $this->isAdmin($user);
    }

    public function update(User $user, Car $car)
    {
        return $this->isAdmin($user);
    }

    public function delete(User $user, Car $car)
    {
        return $this->isAdmin($user);
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function isAdmin(User $user)
    {
        return $user->isAdmin();
    }
}
