<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'model' => 'bail|required|max:255',
            'registration_number' => 'bail|required|alpha_num|size:6',
            'year' => 'bail|required|integer|between:1000,'.date('Y'),
            'color' => 'bail|required|max:255|alpha',
            'price' => 'bail|required|numeric|digits_between:0,255',
        ];
    }
}
