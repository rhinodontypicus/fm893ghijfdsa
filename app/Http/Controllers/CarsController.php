<?php

namespace App\Http\Controllers;

use App\Entity\Car;
use App\Http\Requests\CarRequest;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;

class CarsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('cars.index', ['cars' => Car::all()]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        try {
            $this->authorize('create', Car::class);
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        return view('cars.create', ['car' => (new Car([
            'color' => null,
            'model' => null,
            'registration_number' => null,
            'year' => null,
            'mileage' => null,
            'price' => null,
            'user_id' => null,
        ]))->toArray()]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $car = Car::findOrFail($id);

        return view("cars.show", ['car' => $car->toArray()]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $car = Car::findOrFail($id);

        try {
            $this->authorize('update', $car);
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        return view("cars.edit", ['car' => $car->toArray()]);
    }

    /**
     * @param $id
     * @param CarRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update($id, CarRequest $request)
    {
        $car = Car::findOrFail($id);

        try {
            $this->authorize('update', $car);
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $car->update($request->all());

        return $this->show($id);
    }

    /**
     * @param CarRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store(CarRequest $request)
    {
        try {
            $this->authorize('create', Car::class);
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        Car::create($request->all());

        return $this->index();
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function destroy($id, Request $request)
    {
        $car = Car::findOrFail($id);

        try {
            $this->authorize('delete', $car);
        } catch (AuthorizationException $e) {
            return redirect('/');
        }

        $car->delete();

        return redirect()->route('cars.index');
    }
}
