<?php

namespace App\Http\Controllers\Auth;

use App\Entity\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/cars';

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToGithub()
    {
        return Socialite::driver('github')->redirect();
    }

    public function handleGithubProviderCallback()
    {
        $user = Socialite::driver('github')->user();
        $userToLogin = User::whereEmail($user->email)->first();

        if (!$userToLogin) {
            $userToLogin = User::create([
                'first_name' => $user->name,
                'email' => $user->email,
                'password' => bcrypt(str_random(10)),
            ]);
        }

        auth()->login($userToLogin);

        return redirect('/cars');
    }
}
