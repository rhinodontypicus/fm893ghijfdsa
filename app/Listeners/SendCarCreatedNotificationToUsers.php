<?php

namespace App\Listeners;

use App\Entity\User;
use App\Events\CarCreated;
use App\Notifications\CarCreated as CarCreatedNotification;
use Notification;

class SendCarCreatedNotificationToUsers
{
    /**
     * Handle the event.
     *
     * @param  CarCreated  $event
     * @return void
     */
    public function handle(CarCreated $event)
    {
        Notification::send(User::all(), new CarCreatedNotification($event->car));
    }
}