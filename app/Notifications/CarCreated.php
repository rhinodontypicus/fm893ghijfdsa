<?php

namespace App\Notifications;

use App\Entity\Car;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CarCreated extends Notification implements ShouldQueue
{
    use Queueable;

    public $car;

    /**
     * Create a new notification instance.
     * @param Car $car
     */
    public function __construct(Car $car)
    {
        $this->car = $car;
        $this->onQueue('notification');
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->greeting('New Car Created!')
                    ->subject('New Car Created!')
                    ->line('Model: '.$this->car->model)
                    ->line('Price, $: '.$this->car->price);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
