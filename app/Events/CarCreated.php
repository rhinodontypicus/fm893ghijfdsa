<?php

namespace App\Events;

use App\Entity\Car;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class CarCreated
{
    use Dispatchable, SerializesModels;

    /**
     * @var Car
     */
    public $car;

    /**
     * Create a new event instance.
     * @param Car $car
     */
    public function __construct(Car $car)
    {
        $this->car = $car;
    }
}
