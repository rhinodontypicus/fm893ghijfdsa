@extends('layouts.app')

@section('content')
    <h2>{{ $car['model'] }}</h2>
    <p><strong>Year</strong>: {{ $car['year'] }}</p>
    <p><strong>Registration number</strong>: {{ $car['registration_number'] }}</p>
    <p><strong>Color</strong>: {{ $car['color'] }}</p>
    <p><strong>Price</strong>: ${{ $car['price'] }}</p>

    @can('update', new \App\Entity\Car($car))
        <a href="{{ route('cars.edit', $car['id']) }}" class="btn btn-default edit-button" style="display: inline-block">Edit</a>
    @endcan

    @can('delete', new \App\Entity\Car($car))
        <form action="{{ route('cars.destroy', $car['id']) }}" method="POST" style="display: inline-block">
            {{ method_field('DELETE') }}
            {{ csrf_field() }}
            <button type="submit" class="btn btn-danger delete-button">Delete</button>
        </form>
    @endcan
@endsection